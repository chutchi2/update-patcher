# How does it work?
The file `/update/disableEncryptionUpdate` must exist; if it is found, `AppUpgrade` will proceed to update without encryption

# Benefits? 
* You can customize your OS
* The update is MUCH MUCH MUCH faster!

# How to add stuff to the tars?
It is a bit of a pain adding things on the tar sometimes. This is because they use an old format, and it fails if it is not right... Maybe it was on my old testing due to something else missing... Not sure. In any case, if you have issues check below.

## To make tar.gz 
Go into the folder you want to compress and run the code below
```
tar --format=ustar --numeric-owner -cf - ./ -P | pv -s $(du -sb ./ | awk '{print $1}') -n | gzip > name_of_output.tar.gz
```

## To make a new .tar
Go into the folder you want to compress and run the code below
```
tar --format=ustar --numeric-owner -cf ./ "name_of_output.tar"
```

## To add to existing .tar
Go into the folder you want to compress and run the code below
```
tar --format=ustar --no-fflags --numeric-owner -rvf "mango-name_of_output.tar" "file1" "file2" "path/to/file3" ...
```